#!/bin/bash

cd /tmp
torsocks wget -mpEk -np -e robots=off --wait="1" https://scom.neocities.org
cd /tmp/scom.neocities.org
wget https://66.media.tumblr.com/66928c2517eb0e1fed779f6e5f91a0fb/tumblr_p7s3h5B0UG1ujesf3o5_500.gif
find . -type f -name '*' -exec sed -i 's/https\:\/\/66\.media\.tumblr\.com\/66928c2517eb0e1fed779f6e5f91a0fb//g' {} \;
cat index.html | grep -Ev scom.neocities.org > index.new.html
rm index.html
mv index.new.html index.html
wget https://em-content.zobj.net/source/apple/354/eyes_1f440.png
convert eyes_1f440.png -resize 48x48\! zim.png
rm eyes_1f440.png
zimwriterfs -w index.html -l eng -t "scom.neocities.org" -d "snapshot of scom.neocities.org as of $(date -Idate)" -I zim.png -c "scom.neocities.org" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/scom.neocities.org-$(date -Idate).zim
