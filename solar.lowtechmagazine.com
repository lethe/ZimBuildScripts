#!/bin/bash

cd /tmp
torsocks wget -mpEk -np -e robots=off --wait="1" https://solar.lowtechmagazine.com
cd /tmp/solar.lowtechmagazine.com
find . -type f -name '*' -exec sed -i 's/http\:\/\/solar\.lowtechmagazine\.com\/low-tech-solutions\//\/low-tech-solutions\/index.html/' {} \;
find . -type f -name '*' -exec sed -i 's/http\:\/\/solar\.lowtechmagazine\.com\/high-tech-problems\//\/high-tech-problems\/index.html/' {} \;
find . -type f -name '*' -exec sed -i 's/http\:\/\/solar\.lowtechmagazine\.com\/obsolete-technology\//\/obsolete-technology\/index.html/' {} \;
find . -type f -name '*' -exec sed -i 's/http\:\/\/solar\.lowtechmagazine\.com\/offline-reading\//\/offline-reading\/index.html/' {} \;
find . -type f -name '*' -exec sed -i 's/http\:\/\/solar\.lowtechmagazine\.com\/archives\//\/archives\/index.html/' {} \;
convert icons/sun.svg -resize 48x48\! zim.png
zimwriterfs -w index.html -l eng -t "solar.lowtechmagazine.com" -d "snapshot of solar.lowtechmagazine.com as of $(date -Idate)" -I zim.png -c "solar.lowtechmagazine.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/solar.lowtechmagazine.com-$(date -Idate).zim
