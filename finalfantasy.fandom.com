#!/bin/bash

# Obligatory "I can't seem to scrape images from FANDOM.com" warning.
# I'll manually add a few here as an example.

cd /tmp
# This will take a week at best. (Trust me, I tried.)
# If you're okay with risking getting IP banned
# or you run your own Breezewiki instance
# then modify the following commands as you feel necessary.
#torsocks wget -mpEk -np -e robots=off --wait="1" -R "*edit*" https://antifandom.com/finalfantasy/wiki/
cd /tmp/antifandom.com
curl "https://static.wikia.nocookie.net/finalfantasy/images/c/c6/Dissidia_Warrior_of_Light.png/revision/latest/scale-to-width-down/291?cb=20090407205801" > wol.png
# This sure does take a long time.
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/finalfantasy/images/c/c6/Dissidia_Warrior_of_Light.png/revision/latest/scale-to-width-down/291?cb=20090407205801:/wol.png:g' {} \;
curl "https://static.wikia.nocookie.net/finalfantasy/images/4/4a/Site-favicon.ico/revision/latest?cb=20210601132632" > favicon.ico
convert favicon.ico -resize 48x48\! zim.png
mv zim-0.png zim.png
zimwriterfs -w finalfantasy/wiki/index.html -l eng -t "finalfantasy.fandom.com" -d "snapshot of finalfantasy.fandom.com as of $(date -Idate)" -I zim.png -c "finalfantasy.fandom.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/finalfantasy.fandom.com-$(date -Idate).zim
