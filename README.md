# ZimBuildScripts

Scripts to compress certain websites into a `.zim` file for use with Kiwix. Fetches the newest content from that site's Git repo (or scrapes the website), removes unnecessary files, and stamps the file with the build date.

Dependencies:
- zimwriterfs
- git
- imagemagick
- pandoc

If you have Hyphanet installed and you don't want to run the scripts yourself, you can [download some pre-generated files here](http://127.0.0.1:8888/USK@HEqOWJk7SAhad1sN0QtlPFVl6H19O7Dt~yX~ELuux2o,RCg0LwkCnsq7kI6LH~8b44khKbrFS0iuVZxaOv4n120,AQACAAE/zim4kiwix/-6/).

License: MIT
