#!/bin/bash

cd /tmp
torsocks wget -mpEk -np -e robots=off --wait="1" https://poetry-archive.com
cd /tmp/poetry-archive.com
find . -type f -name '*' -exec sed -i 's/https\:\/\/pagead2\.googlesyndication\.com\/pagead\/js\/adsbygoogle\.js//g' {} \;
find . -type f -name '*' -exec sed -i 's/http\:\/\/pagead2\.googlesyndication\.com\/pagead\/show\_ads\.js//' {} \;
find . -type f -name '*' -exec sed -i 's/http\:\/\/tags\.expo9\.exponential.com\/tags\/Everypoetcom\/ROS\/tags\.js//' {} \;
find . -type f -name '*' -exec sed -i 's/http\:\/\/www\.google\-analytics\.com\/urchin\.js//g' {} \;
find . -type f -name '*' -exec sed -i 's/as.casalemedia.com//' {} \;
find . -type f -name '*' -exec sed -i 's/i.po.st//' {} \;
find . -type f -name '*' -exec sed -i 's/http\:\/\/\/s//' {} \;
find . -type f -name '*' -exec sed -i 's/www.burstnet.com//' {} \;
convert p_pic.gif -resize 48x48\! zim.png
zimwriterfs -w index.html -l eng -t "poetry-archive.com" -d "snapshot of poetry-archive.com as of $(date -Idate)" -I zim.png -c "poetry-archive.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/poetry-archive.com-$(date -Idate).zim

