#!/bin/bash

# Obligatory "I can't seem to scrape images from FANDOM.com" warning.
# I'll manually add a few here as an example.

cd /tmp
torsocks wget -mpEk -np -e robots=off --wait="1" -R "*edit*" https://breeze.mint.lgbt/xenoblade/wiki
cd /tmp/breeze.mint.lgbt
curl "https://static.wikia.nocookie.net/xenoblade/images/0/03/Fiora_pic.png/revision/latest/scale-to-width-down/350?cb=20201029195835" > fiora.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/xenoblade/images/0/03/Fiora_pic.png/revision/latest/scale-to-width-down/350?cb=20201029195835:/fiora.png:g' {} \;
curl "https://static.wikia.nocookie.net/xenoblade/images/b/b8/XCDE-Fiora_Portrait.png/revision/latest/scale-to-width-down/350?cb=20200715014915" > fiora_portrait.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/xenoblade/images/b/b8/XCDE-Fiora_Portrait.png/revision/latest/scale-to-width-down/350?cb=20200715014915:/fiora_portrait.png:g' {} \;
curl "https://static.wikia.nocookie.net/xenoblade/images/e/e4/Fiora_in_Face_Nemesis.png/revision/latest/scale-to-width-down/180?cb=20150921134825" > fiora_face.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/xenoblade/images/e/e4/Fiora_in_Face_Nemesis.png/revision/latest/scale-to-width-down/180?cb=20150921134825:/fiora_face.png:g' {} \;
curl "https://static.wikia.nocookie.net/xenoblade/images/c/c6/Nemesisfiora.png/revision/latest/scale-to-width-down/180?cb=20201224164444" > nemesis_fiora.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/xenoblade/images/c/c6/Nemesisfiora.png/revision/latest/scale-to-width-down/180?cb=20201224164444:/nemesis_fiora.png:g' {} \;
curl "https://static.wikia.nocookie.net/xenoblade/images/4/4a/Site-favicon.ico/revision/latest?cb=20210602214446" > favicon.ico
convert favicon.ico -resize 48x48\! zim.png
zimwriterfs -w xenoblade/wiki/index.html -l eng -t "xenoblade.fandom.com" -d "snapshot of xenoblade.fandom.com as of $(date -Idate)" -I zim.png -c "xenoblade.fandom.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/xenoblade.fandom.com-$(date -Idate).zim
