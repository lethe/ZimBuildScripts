#!/bin/bash

cd /tmp
torsocks wget -mpEk -np -e robots=off --wait="1" https://drewdevault.com
cd /tmp/drewdevault.com
find . -type f -name '*' -exec sed -i 's/https\:\/\/drewdevault.com//g' {} \;
convert avatar.png -resize 48x48\! zim.png
zimwriterfs -w index.html -l eng -t "drewdevault.com" -d "snapshot of drewdevault.com as of $(date -Idate)" -I zim.png -c "drewdevault.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/drewdevault.com-$(date -Idate).zim
