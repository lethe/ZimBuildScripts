#!/bin/bash

# Some functions of this site are still broken.

cd /tmp
torsocks wget -mpEk -np -e robots=off --wait="1" http://everypoet.com
cd /tmp/everypoet.com
find . -type f -name '*' -exec sed -i 's/https\:\/\/pagead2\.googlesyndication\.com\/pagead\/js\/adsbygoogle\.js//g' {} \;
find . -type f -name '*' -exec sed -i 's/http\:\/\/pagead2\.googlesyndication\.com\/pagead\/show\_ads\.js//' {} \;
find . -type f -name '*' -exec sed -i 's/http\:\/\/tags\.expo9\.exponential.com\/tags\/Everypoetcom\/ROS\/tags\.js//' {} \;
find . -type f -name '*' -exec sed -i 's/http\:\/\/www\.google\-analytics\.com\/urchin\.js//g' {} \;
find . -type f -name '*' -exec sed -i 's/a\.tribalfusion\.com//' {} \;
find . -type f -name '*' -exec sed -i 's/www\.burstnet\.com//' {} \;
find . -type f -name '*' -exec sed -i 's/media\.fastclick\.net//' {} \;
find . -type f -name '*' -exec sed -i 's/cgi-bin//' {} \;
find . -type f -name '*' -exec sed -i 's/www\.everypoet\.net//' {} \;
rm -r links
find . -type f -name '*' -exec sed -i 's/null.invalid\/links//' {} \;
rm poetryforum.htm
find . -type f -name '*' -exec sed -i 's/poetryforum.htm//' {} \;
rm absurdities/elements/submit.htm
find . -type f -name '*' -exec sed -i 's/submit.htm//' {} \;
rm pffa.htm
find . -type f -name '*' -exec sed -i 's/pffa.htm//' {} \;
convert images/cafepress/mug-thumbnail.jpg -resize 48x48\! zim.png
zimwriterfs -w index.html -l eng -t "everypoet.com" -d "snapshot of everypoet.com as of $(date -Idate)" -I zim.png -c "everypoet.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/everypoet.com-$(date -Idate).zim
