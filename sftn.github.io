#!/bin/bash

cd /tmp
git clone --depth 1 https://github.com/sftn/sftn.github.io
cd /tmp/sftn.github.io
convert img/index.png -resize 48x48\! ./zim.png
rm -rf /tmp/sftn.github.io/.git
echo $PWD
zimwriterfs -w index.html -l eng -t "sftn.github.io" -d "snapshot of sftn.github.io as of $(date -Idate)" -I zim.png -c "sftn.github.io" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/sftn.github.io-$(date -Idate).zim
