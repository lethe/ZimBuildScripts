#!/bin/bash

cd /tmp
torsocks wget -mpEk -np -e robots=off --wait="1" http://n-gate.com
cd /tmp/n-gate.com
# The privacy policy page seems to be missing on purpose.
# I got an HTTP 451 error when attempting to manually access it.
find . -type f -name '*' -exec sed -i 's/http\:\/\/n-gate.com//' {} \;
convert favicon.ico -resize 48x48\! zim.png
zimwriterfs -w index.html -l eng -t "n-gate.com" -d "snapshot of n-gate.com as of $(date -Idate)" -I zim.png -c "n-gate.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/n-gate.com-$(date -Idate).zim
