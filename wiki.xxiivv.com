#!/bin/bash

cd /tmp
git clone --depth 1 https://github.com/XXIIVV/oscean
cd /tmp/oscean
convert media/icon/logo.oscean.black.png -resize 48x48\! zim.png
rm -rf .git
zimwriterfs -w index.html -l eng -t "wiki.xxiivv.com" -d "snapshot of wiki.xxiivv.com as of $(date -Idate)" -I zim.png -c "wiki.xxiivv.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/wiki.xxiivv.com-$(date -Idate).zim
