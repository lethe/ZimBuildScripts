#!/bin/bash

cd /tmp
git clone --depth 1 https://github.com/mmcgrana/gobyexample /tmp/gobyexample
wget https://raw.githubusercontent.com/egonelbre/gophers/master/vector/projects/network.svg
convert network.svg -resize 48x48\! zim.png
mv /tmp/zim.png /tmp/gobyexample/public/
rm -rf /tmp/gobyexample/.git
cd /tmp/gobyexample/public/
echo $PWD
zimwriterfs -w index.html -l eng -t "gobyexample.com" -d "snapshot of gobyexample.com as of $(date -Idate)" -I zim.png -c "gobyexample.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/gobyexample.com-$(date -Idate).zim
