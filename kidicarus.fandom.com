#!/bin/bash

# We're actually going to scrape a Breezewiki instance instead.
# I can't seem to get images working, so we'll manually add a few later.
# Please be warned the desktop version of Kiwix will fetch the missing
# images from FANDOM.com's servers if you have an Internet connection.
# Decide on your own if this is a privacy hazard.

cd /tmp
#torsocks wget -mpEk -np -e robots=off --wait="1" -R "*edit*" https://antifandom.com/kidicarus/wiki/
cd /tmp/antifandom.com

# Cleanup: here is where we will manually add some images
curl "https://static.wikia.nocookie.net/kidicarus/images/d/d6/Darkpitart.png/revision/latest?cb=20160510022613" > dark_pit.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/kidicarus/images/d/d6/Darkpitart.png/revision/latest/scale-to-width-down/300?cb=20160510022613:/dark_pit.png:g' {} \;
curl "https://static.wikia.nocookie.net/kidicarus/images/9/92/Andiamdarkpit.ogg/revision/latest?cb=20160715024654" > dp.ogg
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/kidicarus/images/9/92/Andiamdarkpit.ogg/revision/latest?cb=20160715024654:/dp.ogg:g' {} \;
curl "https://static.wikia.nocookie.net/kidicarus/images/1/13/Unaffiliated_Mini_Icon.png/revision/latest?cb=20160418011007" > unaffiliated.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/kidicarus/images/1/13/Unaffiliated_Mini_Icon.png/revision/latest?cb=20160418011007:/unaffiliated.png:g' {} \;
curl "https://static.wikia.nocookie.net/kidicarus/images/d/d6/Darkpitart.png/revision/latest/scale-to-width-down/300?cb=20160510022613" > dark_pit2.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/kidicarus/images/d/d6/Darkpitart.png/revision/latest/scale-to-width-down/300?cb=20160510022613:/dark_pit2.png:g' {} \;
curl "https://static.wikia.nocookie.net/kidicarus/images/8/8f/Darkpitconceptart.png/revision/latest/scale-to-width-down/180?cb=20160410215346" > dark_pit_concept.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/kidicarus/images/8/8f/Darkpitconceptart.png/revision/latest/scale-to-width-down/180?cb=20160410215346:/dark_pit_concept.png:g' {} \;

# Cleanup: add icons in this section
curl "https://static.wikia.nocookie.net/kidicarus/images/1/13/Unaffiliated_Mini_Icon.png/revision/latest?cb=20160418011007" > icon_unaffiliated.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/kidicarus/images/1/13/Unaffiliated_Mini_Icon.png/revision/latest?cb=20160418011007:/icon_unaffiliated.png:g' {} \;
curl "https://static.wikia.nocookie.net/kidicarus/images/5/5d/Forces_of_Nature_Mini_Icon.png/revision/latest?cb=20120901183452" > icon_nature.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/kidicarus/images/5/5d/Forces_of_Nature_Mini_Icon.png/revision/latest?cb=20120901183452:/icon_nature.png:g' {} \;
curl "https://static.wikia.nocookie.net/kidicarus/images/3/32/DarkPitWingIcon.png/revision/latest?cb=20150922132311" > wing_dp.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/kidicarus/images/3/32/DarkPitWingIcon.png/revision/latest?cb=20150922132311:/wing_dp.png:g' {} \;

# Cleanup: add AR cards in this section
echo "[INFO] adding AR cards"
echo "[INFO]                 ...ar_darkpit.png"
mkdir img
cd img
curl --silent "https://static.wikia.nocookie.net/kidicarus/images/7/77/Dark_Pit.jpg/revision/latest/scale-to-width-down/250?cb=20120408204637" > ar_darkpit.png
cd ../
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/kidicarus/images/7/77/Dark_Pit.jpg/revision/latest/scale-to-width-down/250?cb=20120408204637:/img/ar_darkpit.png:g' {} \;
echo "[INFO]                 ...ar_pit.png"
cd img
curl --silent "https://static.wikia.nocookie.net/kidicarus/images/3/3c/Pitarcard.png/revision/latest/scale-to-width-down/180?cb=20160704003045" > ar_pit.png
cd ../
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/kidicarus/images/3/3c/Pitarcard.png/revision/latest/scale-to-width-down/180?cb=20160704003045:/img/ar_pit.png:g' {} \;
echo "[INFO]                 ...ar_palutena.png"
cd img
curl --silent "https://static.wikia.nocookie.net/kidicarus/images/b/b4/AKD-E002.png/revision/latest/scale-to-width-down/250?cb=20151228180325" > ar_palutena.png
cd ../
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/kidicarus/images/b/b4/AKD-E002.png/revision/latest/scale-to-width-down/250?cb=20151228180325:/img/ar_palutena.png:g' {} \;

cd /tmp/antifandom.com
curl "https://static.wikia.nocookie.net/kidicarus/images/4/4a/Site-favicon.ico/revision/latest?cb=20210713134243" > favicon.ico
convert favicon.ico -resize 48x48\! zim.png
zimwriterfs -w kidicarus/wiki/index.html -l eng -t "kidicarus.fandom.com" -d "snapshot of kidicarus.fandom.com as of $(date -Idate)" -I zim.png -c "kidicarus.fandom.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/kidicarus.fandom.com-$(date -Idate).zim
