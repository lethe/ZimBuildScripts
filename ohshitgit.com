#!/bin/bash

cd /tmp
git clone --depth 1 https://github.com/ksylor/ohshitgit
mkdir /tmp/staging
cd /tmp/ohshitgit/en/noswears/tips/
for i in *.md
do
	touch /tmp/staging/$i.html
	echo "<title>" > /tmp/staging/$i.html
	cat $i | grep title | sed -e 's/title: //g' >> /tmp/staging/$i.html
	echo "</title>" >> /tmp/staging/$i.html
	echo "<h1>" >> /tmp/staging/$i.html
	cat $i | grep title | sed -e 's/title: //g' >> /tmp/staging/$i.html
	echo "</h1>" >> /tmp/staging/$i.html
	cat $i | tail -n +7 | pandoc -f markdown -t html --ascii >> /tmp/staging/$i.html
done
cd /tmp/staging/
wget https://git-scm.com/images/logos/downloads/Git-Icon-1788C.svg
convert Git-Icon-1788C.svg -resize 48x48\! zim.png
tree -H . > index.html
find . -type f -name '*' -exec sed -i "s/’/'/g" {} \;
echo $PWD
zimwriterfs -w index.html -l eng -t "ohshitgit.com" -d "snapshot of ohshitgit.com as of $(date -Idate)" -I zim.png -c "ohshitgit.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/ohshitgit.com-$(date -Idate).zim
