#!/bin/bash

# Obligatory "I can't seem to scrape images from FANDOM.com" warning.
# I'll manually add a few here as an example.

cd /tmp
torsocks wget -mpEk -np -e robots=off --wait="1" -R "*edit*" https://bw.projectsegfau.lt/fireemblem/wiki/
cd /tmp/bw.projectsegfau.lt
curl "https://static.wikia.nocookie.net/fireemblem/images/d/d0/Lucina_official_art.png/revision/latest/scale-to-width-down/350?cb=20191103195341" > lucina.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/fireemblem/images/d/d0/Lucina_official_art.png/revision/latest/scale-to-width-down/350?cb=20191103195341:/lucina.png:g' {} \;
curl "https://static.wikia.nocookie.net/fireemblem/images/7/73/Warriors_Lucina_OA.png/revision/latest/scale-to-width-down/349?cb=20171018094414" > lucina_warriors.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/fireemblem/images/7/73/Warriors_Lucina_OA.png/revision/latest/scale-to-width-down/349?cb=20171018094414:/lucina_warriors.png:g' {} \;
curl "https://static.wikia.nocookie.net/fireemblem/images/4/4b/Anna_fates_portrait.png/revision/latest/scale-to-width-down/60?cb=20170120221806" > anna_spoliers.png
find . -type f -name '*' -exec sed -i 's:https\://static.wikia.nocookie.net/fireemblem/images/4/4b/Anna_fates_portrait.png/revision/latest/scale-to-width-down/60?cb=20170120221806:/anna_spoilers.png:g' {} \;
curl "https://static.wikia.nocookie.net/fireemblem/images/4/4a/Site-favicon.ico/revision/latest?cb=20210702135510" > favicon.ico
convert favicon.ico -resize 48x48\! zim.png
zimwriterfs -w fireemblem/wiki/index.html -l eng -t "fireemblem.fandom.com" -d "snapshot of fireemblem.fandom.com as of $(date -Idate)" -I zim.png -c "fireemblem.fandom.com" -p "https://codeberg.org/lethe/ZimBuildScripts" $PWD /tmp/fireemblem.fandom.com-$(date -Idate).zim
